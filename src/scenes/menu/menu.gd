extends Control


func _ready():
	%'StartButton'.pressed.connect(_on_start_pressed)
	%'SettingsButton'.pressed.connect(_on_settings_pressed)
	%'UserDirButton'.pressed.connect(_on_user_dir_pressed)

	%'VersionLabel'.text = 'Version: %s'%[
		VERSION.COMMIT
	]
	
	if INPUTSTATE.get_is_input_via_joypad():
		%'StartButton'.grab_focus.call_deferred()


func _on_start_pressed() -> void:
	STATE.reset()
	SCENE.goto_scene(ID.SCENE_TYPES.STORY_WALKING)


func _on_settings_pressed() -> void:
	SCENE.goto_scene(ID.SCENE_TYPES.SETTING)


func _on_user_dir_pressed() -> void:
	OS.shell_open(
		OS.get_user_data_dir()
	)
