extends Node2D

@onready var ap:AnimationPlayer = %AnimationPlayer
@onready var label:Label = %Label

func _remove() -> void:
	queue_free()
	
func set_values_and_animate(value:String, start_pos:Vector2, height:float, spread:float) -> void:
	label.text = value
	ap.play("hit_label")
	
	var tween = get_tree().create_tween()
	var end_pos = Vector2(randf_range(-spread,spread),-height) + start_pos
	var tween_length = ap.get_animation("hit_label").length
	
	tween.tween_property(label,"position",end_pos,tween_length).from(start_pos)
