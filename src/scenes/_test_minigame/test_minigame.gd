extends Node


@onready var _music_mate: MusicMate = %MusicMate
@onready var _scoring_mate: ScoringMate = %ScoringMate


func _ready():
	_music_mate.note_start.connect(_on_note_start)
	_music_mate.note_end.connect(_on_note_end)

	_scoring_mate.hit_perfect.connect(_on_hit_perfect)
	# todo: other signals


func _on_note_start(_note) -> void:
	pass # todo


func _on_note_end(_note) -> void:
	pass # todo


func _on_hit_perfect() -> void:
	pass # todo
