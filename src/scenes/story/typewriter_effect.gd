extends RichTextLabel

var story_finished: bool = false
var tween: Tween
var sparkle_effect: SparkleRichTextEffect

func _ready():
	
	sparkle_effect = SparkleRichTextEffect.new()
	custom_effects.clear()
	install_effect(sparkle_effect)
	
	var charCount = text.length()
	var writingDuration = charCount / 30.0
	
	tween = get_tree().create_tween()
	tween.tween_property(self, "visible_ratio", 1.0, writingDuration).from(0.0)
	await tween.finished
	story_finished = true


func _input(event):
	if event.is_action_pressed("Story_Continue"):
		if story_finished:
			SCENE.goto_scene(ID.SCENE_TYPES._TEST_MINIGAME)
		else:
			tween.stop()
			visible_ratio = 1
			story_finished = true
