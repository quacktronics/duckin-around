extends CanvasLayer


@onready var _fullscreen_view = %FullscreenView
@onready var _overlays_view = %OverlaysView

@onready var _values = %Values
@onready var _dev_console = %DevConsole
@onready var _actions = %Actions
@onready var _overlay_values = %OverlayValues


func _ready():
	_fullscreen_view.hide()
	_overlays_view.show()

	_values.hide()
	_actions.hide()
	_overlay_values.hide()

	SCENE.removed_old_scene.connect(_on_removed_old_scene)


func _input(__event):
	if __event.is_action_pressed('debug_dev_console'):
		if _fullscreen_view.visible:
			_switch_to_overlay_view()
		else:
			_switch_to_fullscreen_view()

	elif __event.is_action_pressed('pause_menu') and _fullscreen_view.visible:
		get_viewport().set_input_as_handled()
		_switch_to_overlay_view()


func add_console_cmd(__cmd: String, __f: Callable) -> void:
	_dev_console.add_console_cmd(__cmd, __f)


func add_console_log_msg(__text: String, __color: Color = Color.WHITE) -> void:
	_dev_console.add_log_msg(__text, __color)


func add_action_button(__text: String, __f: Callable) -> void:
	_actions.add_action_button(__text, __f)
	if _actions.hidden:
		_actions.show()


func add_value_header(__label_text: String, __show_in_overlay: bool = false) -> void:
	_values.add_value_header(__label_text)
	if _values.hidden:
		_values.show()

	if __show_in_overlay:
		_overlay_values.add_value_header(__label_text)
		if _overlay_values.hidden:
			_overlay_values.show()


func add_value(
	__label_text: String, __obj: Variant, __expression_str: String, __show_in_overlay: bool = false
) -> void:
	_values.add_value(__label_text, __obj, __expression_str)
	if _values.hidden:
		_values.show()
		
	if __show_in_overlay:
		_overlay_values.add_value(__label_text, __obj, __expression_str)
		if _overlay_values.hidden:
			_overlay_values.show()


func one_shot_value(__text: String) -> void:
	_overlay_values.one_shot_value(__text)
	if _overlay_values.hidden:
		_overlay_values.show()


func _on_removed_old_scene(__old_scene_id: ID.SCENE_TYPES, __new_scene_id: ID.SCENE_TYPES) -> void:
	_dev_console.clear()
	_values.clear()
	_actions.clear()
	_overlay_values.clear()

	_values.hide()
	_actions.hide()
	_overlay_values.hide()


func _switch_to_fullscreen_view() -> void:
	get_tree().paused = true

	_overlays_view.hide()
	_fullscreen_view.show()

	_dev_console.grab_focus_for_cmd_edit()


func _switch_to_overlay_view() -> void:
	get_tree().paused = false

	_dev_console.clean_up_for_hide()

	_fullscreen_view.hide()
	_overlays_view.show()
