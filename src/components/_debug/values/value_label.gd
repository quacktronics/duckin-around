extends HBoxContainer


@onready var _label_label = %LabelLabel
@onready var _value_label = %ValueLabel


func set_data(__label_text: String, __value: Variant) -> void:
	_label_label.set_text(__label_text)
	
	match typeof(__value):
		TYPE_NIL:
			_value_label.set_text('<null>')
		TYPE_STRING:
			_value_label.set_text(__value)
		TYPE_INT, TYPE_FLOAT, TYPE_BOOL:
			_value_label.set_text(str(__value))
		TYPE_VECTOR2, TYPE_VECTOR2I, TYPE_VECTOR3, TYPE_VECTOR3I, TYPE_VECTOR4, TYPE_VECTOR4I:
			_value_label.set_text(str(__value))
		TYPE_COLOR:
			_value_label.set_text('r=%d, g=%d, b=%d, a=%d'%[
				__value.r, __value.g, __value.b, __value.a
			])
		#TYPE_ARRAY:
		#TYPE_DICTIONARY:
		#TYPE_OBJECT:
		_:
			if UtilService.is_debug():
				push_error("Not defined how to render a value of type " + str(typeof(__value)))
