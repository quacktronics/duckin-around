class_name MusicMate extends Node


@export var audio: AudioStream
@export var midi: MidiResource
@export var auto_play_delay := 4.0
@export var auto_play := true


signal process_audio_time(delta)
signal note_start(note)
signal note_end(note)


@onready var _audio_latency = AudioServer.get_output_latency()


var _logger = LOGGING.get_logger('MusicMate')

var _audio_player: AudioStreamPlayer
var _midi_player: MidiPlayer

var _has_playing_started := false
var _has_playing_finished := false
var _start_time := 0.0
var _last_time := 0.0


func _ready():
	if not audio:
		_logger.error('No audio set, aborting!')
		queue_free()
		return

	if not midi:
		_logger.error('No midi set, aborting!')
		queue_free()
		return

	_audio_player = AudioStreamPlayer.new()
	_audio_player.finished.connect(_on_audio_finished)
	add_child(_audio_player)

	_midi_player = MidiPlayer.new()
	add_child(_midi_player)
	_midi_player.stop() # quick fix, otherwise first note gets emitted immediately
	_midi_player.manual_process = true
	_midi_player.note.connect(_on_midi_note)

	_audio_player.stream = audio
	_midi_player.midi = midi

	_start_time = Time.get_ticks_msec()


func _process(__delta):
	if _has_playing_finished:
		return

	if not _has_playing_started:
		if not auto_play:
			return

		var absolute_time = Time.get_ticks_msec()
		var relative_time = (absolute_time - _start_time) / 1_000
		@warning_ignore('INTEGER_DIVISION') # we only care about seconds
		var delay_since_start = relative_time
		if delay_since_start >= auto_play_delay:
			play()
			_has_playing_started = true
		else:
			_last_time = relative_time
			process_audio_time.emit(_last_time)
			return

	var time = (
		auto_play_delay
		+ _audio_player.get_playback_position()
		+ AudioServer.get_time_since_last_mix()
		- _audio_latency
	)

	var asp_delta = time - _last_time
	_last_time = time

	process_audio_time.emit(time)
	_midi_player.process_delta(asp_delta)


func _on_audio_finished():
	_has_playing_finished = true
	


func _on_midi_note(__event, __track):
	if (
		_has_playing_started
		and not _has_playing_finished
		and __event["type"] == "note"
	):
		if (__event['subtype'] == MIDI_MESSAGE_NOTE_ON):
			note_start.emit(__event['note'])
		elif (__event['subtype'] == MIDI_MESSAGE_NOTE_OFF):
			note_end.emit(__event['note'])


func play() -> void:
	_audio_player.play()
	_midi_player.play()
