extends CenterContainer


@export var time := 3.0
@export var final_text := 'Go!'
@export var colors: Array[Color] = [
	Color.from_string('#FA6800', Color.WHITE),
	Color.from_string('#F0A30A', Color.WHITE),
	Color.from_string('#EEE80A', Color.WHITE),
	Color.from_string('#A4C400', Color.WHITE),
]


@onready var _label: Label = %Label
@onready var _animation_player: AnimationPlayer = %AnimationPlayer

@onready var _remaining_time := time

var _logger := LOGGING.get_logger('Countdown')


func _ready():
	if len(colors) < time + 1:
		_logger.error('The color array needs %d entries, but only %d were set!'%[
			time + 1, len(colors)
		])
		hide()
		return

	loop()


func loop() -> void:
	if _remaining_time > 0.0:
		_label.set_text('%d...'%_remaining_time)
		_label.add_theme_color_override('font_color', colors[int(_remaining_time + 1) * -1])
		_animation_player.seek(0.0)
		_animation_player.play('bounce')
	elif is_zero_approx(_remaining_time):
		_label.set_text(final_text)
		_label.add_theme_color_override('font_color', colors[-1])
		_animation_player.play('bounce_last')
	else:
		return

	await get_tree().create_timer(1.0).timeout
	_remaining_time -= 1.0
	loop()
