extends Control


@export var scoring_mate: ScoringMate

@onready var spawn_point = %SpawnPoint
@onready var hit_label = preload("res://scenes/_test_minigame/hit_feedback.tscn")


var _logger = LOGGING.get_logger('ScroingView')


func _ready():
	if not scoring_mate:
		_logger.error('No ScoringMate set, aborting!')
		queue_free()
		return
	
	scoring_mate.hit_perfect.connect(_on_hit_perfect)
	scoring_mate.hit_too_early.connect(_on_hit_early)
	scoring_mate.miss_too_early.connect(_on_missed)
	scoring_mate.hit_too_late.connect(_on_hit_late)
	scoring_mate.miss_too_late.connect(_on_missed)


func _on_hit_perfect() -> void:
	print('PERFECT, YAY ^^') # todo
	spawn_hit_label('PERFECT, YAY ^^')
	
func _on_hit_early() -> void:
	spawn_hit_label(DATA.hit_too_early_utterance)
	
func _on_hit_late() -> void:
	spawn_hit_label(DATA.hit_too_late_utterance)
	
func _on_missed() -> void:
	spawn_hit_label(DATA.missed_utterance)

func spawn_hit_label(value:String):
	var pos = spawn_point.position
	var height = 300
	var spread = 300
	
	var object = hit_label.instantiate()
	add_child(object)
	object.set_values_and_animate(value, pos, height, spread)
