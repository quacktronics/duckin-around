class_name ScoringMate extends Node


@export var music_mate: MusicMate

signal miss_too_early()
signal hit_too_early()
signal hit_perfect()
signal hit_too_late()
signal miss_too_late()

var _logger = LOGGING.get_logger('ScoringMate')

var _note_starts: Array[int] = []
var _length: int

var _last_time: int
var _current_note_idx: int = 0
var _has_hit_current_note := false

var _miss_too_early_count := 0
var _hit_too_early_count := 0
var _hit_perfect_count := 0
var _hit_too_late_count := 0
var _miss_too_late_count := 0
var _total_score := 0

var _is_ready := false
var _is_done := false


"""
⚠ All times in this class are handled in milliseconds (instead of fractions of seconds)
"""


func _ready():
	if not music_mate:
		_logger.error('No MusicMate set, aborting!')
		queue_free()
		return

	music_mate.process_audio_time.connect(_on_process_audio_time)

	var midi = music_mate.midi
	for track in midi.tracks:
		for event in track.events:
			if event["type"] == "note":
				var time_in_msec := int((event['time'] + music_mate.auto_play_delay) * 1_000)
				if event["subtype"] == MIDI_MESSAGE_NOTE_ON:
					_note_starts.append(time_in_msec)
				elif event["subtype"] == MIDI_MESSAGE_NOTE_OFF:
					_length = time_in_msec
	
	_is_ready = true


func _on_process_audio_time(__time):
	if not _is_ready or _is_done:
		return

	if _current_note_idx == len(_note_starts):
		_emit_result()
		_is_done = true
		return

	_last_time = __time * 1_000

	var current_note_edge = _note_starts[_current_note_idx]
	var current_note_end = current_note_edge + DATA.score_windows[-1]
	var hit_note_has_passed = _has_hit_current_note and _last_time >= current_note_end
	
	var has_missed_start = false
	if not hit_note_has_passed and _current_note_idx < len(_note_starts) - 1:
		var next_note_edge = _note_starts[_current_note_idx + 1]
		var next_note_start = next_note_edge - DATA.score_windows[-1]
		has_missed_start = _last_time >= next_note_start

	if hit_note_has_passed or has_missed_start:
		_current_note_idx += 1
		_has_hit_current_note = false


func _input(__event):
	if not _is_ready or _is_done:
		return

	if __event is InputEventMouseButton and __event.pressed:
		_handle_pressed()


func _unhandled_input(__event):
	if not _is_ready or _is_done:
		return

	if (
		(__event is InputEventKey or __event is InputEventJoypadButton)
		and __event.pressed
		and not __event.is_echo()
	):
		_handle_pressed()


func _handle_pressed() -> void:
	var current_note_edge = _note_starts[_current_note_idx]
	var current_note_start = current_note_edge - DATA.score_windows[-1]
	var current_note_end = current_note_edge + DATA.score_windows[-1]

	if _has_hit_current_note:
		# = double hit on the same note
		_log('🔹 ignored           | t=%d | idx=%d | %d'%[
			_last_time, _current_note_idx,
			current_note_edge
		])

	elif _last_time < current_note_start:
		# = miss - early
		_has_hit_current_note = true # well, tried to hit, but still counts

		_miss_too_early_count += 1
		miss_too_early.emit()

		_log('💥 missed - early    | t=%d | idx=%d | %d'%[
			_last_time, _current_note_idx,
			current_note_edge
		])

	elif _last_time <= current_note_end:
		# = hit
		_has_hit_current_note = true

		var note_score = 0
		var time_delta = current_note_edge - _last_time
		if abs(time_delta) < DATA.score_windows[0]:
			_hit_perfect_count += 1
			hit_perfect.emit()
			note_score = DATA.score_values[0]
		elif abs(time_delta) < DATA.score_windows[1]:
			if time_delta > 0:
				_hit_too_late_count += 1
				hit_too_late.emit()
			else:
				_hit_too_early_count += 1
				hit_too_early.emit()
			note_score = DATA.score_values[1]
		else:
			if time_delta > 0:
				_hit_too_late_count += 1
				hit_too_late.emit()
			else:
				_hit_too_early_count += 1
				hit_too_early.emit()
			note_score = DATA.score_values[2]

		_total_score += note_score

		_log('🟢 hit               | t=%d | idx=%d | %d | d=%d | +%d'%[
			_last_time, _current_note_idx,
			current_note_edge,
			time_delta, note_score
		])

	else:
		# = miss - late
		_has_hit_current_note = true # well, tried to hit, but still counts

		_miss_too_late_count += 1
		miss_too_late.emit()

		_log('💥 missed - late     | t=%d | idx=%d | %d'%[
			_last_time, _current_note_idx,
			current_note_edge
		])


func _count_to_percent (count: int) -> int: 
	return int(round( (float(count) / len(_note_starts)) * 100.0 ))


func _emit_result() -> void:
	var miss_too_early_percent = _count_to_percent(_miss_too_early_count)
	var hit_too_early_percent = _count_to_percent(_hit_too_early_count)
	var hit_perfect_percent = _count_to_percent(_hit_perfect_count)
	var hit_too_late_percent = _count_to_percent(_hit_too_late_count)
	var miss_too_late_percent = _count_to_percent(_miss_too_late_count)

	var score = round(float(_total_score) / len(_note_starts))
	var grade := ''
	var ducklings := 0

	for rating_idx in len(DATA.rating_windows):
		if score >= DATA.rating_windows[rating_idx]:
			grade = DATA.rating_grades[rating_idx]
			ducklings = DATA.rating_ducklings[rating_idx]
			break

	var debug_str = (
		'🏁 done!'
		+ '\n\tscore = %d   grade = %s'%[score, grade]
		+ '\n\tmiss early = %d%%'%miss_too_early_percent
		+ '\n\thit early  = %d%%'%hit_too_early_percent
		+ '\n\t✨ perfect = %d%%'%hit_perfect_percent
		+ '\n\thit late   = %d%%'%hit_too_late_percent
		+ '\n\tmiss late  = %d%%'%miss_too_late_percent
		+ '\n\tducklings  = +%d'%ducklings
	)

	_logger.info(debug_str)
	DEBUG.one_shot_value(debug_str)


func _log(__txt: String) -> void:
	_logger.debug(__txt)
	DEBUG.one_shot_value(__txt)
