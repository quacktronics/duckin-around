class_name MusicVisualiser extends Control


@export var music_mate: MusicMate
@export var long_note_threshold := 2.0


const _background_scene = preload('res://components/music_visualiser/mv_background.tscn')
const _overlay_scene = preload('res://components/music_visualiser/mv_overlay.tscn')
const _note_small_scene = preload('res://components/music_visualiser/mv_note_small.tscn')


var _logger := LOGGING.get_logger('MusicVisualiser')

var _second_to_px_scale: int = 0
var _length := 0.0

var _notes_anchor: Control


func _ready():
	if not music_mate:
		_logger.error('No MusicMate set, aborting!')
		queue_free()
		return

	music_mate.process_audio_time.connect(_on_process_audio_time)

	_second_to_px_scale = int(round(size.y / (music_mate.auto_play_delay + 1.0)))
	_logger.info('scaling: 1 sec = %d px'%_second_to_px_scale)

	var background = _background_scene.instantiate()
	add_child(background)
	var note_x = background.size.x / 2

	_notes_anchor = Control.new()
	add_child(_notes_anchor)

	var overlay = _overlay_scene.instantiate()
	add_child(overlay)
	overlay.set_position(Vector2(0, music_mate.auto_play_delay * _second_to_px_scale))

	var midi = music_mate.midi
	for track in midi.tracks:
		for event in track.events:
			if event["type"] == "note":
				if event["subtype"] == MIDI_MESSAGE_NOTE_ON:
					var note = _note_small_scene.instantiate()
					_notes_anchor.add_child(note)
					note.second_to_px_scale = _second_to_px_scale
					note.set_position(Vector2(
						note_x,
						event['time'] * _second_to_px_scale * -1
					))
				elif event["subtype"] == MIDI_MESSAGE_NOTE_OFF:
					_length = event['time']


func _on_process_audio_time(__time):
	_notes_anchor.position.y = __time * _second_to_px_scale
