extends Control

@export var draw_debug: bool = true
@export var second_to_px_scale: int = 1


func _process(__delta):
	if draw_debug and UtilService.is_debug():
		queue_redraw()

func _draw_debug_rect(__score_window: int, __color: Color) -> void:
	@warning_ignore('INTEGER_DIVISION') # we only care about seconds
	var rect_length = __score_window * second_to_px_scale / 1_000
	var rect_start = Vector2(-1 * rect_length, -1 * rect_length)
	var rect_size = Vector2(2 * rect_length, 2 * rect_length)
	draw_rect(
		Rect2(rect_start, rect_size),
		__color,
		false
	)
