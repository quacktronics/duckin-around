class_name MainPauseMenuScreen extends CenterContainer


signal continued()
signal exited()


@onready var _continue_button: Button = %ContinueButton
@onready var _exit_button: Button = %ExitButton


func _ready():
	visibility_changed.connect(_on_visibility_changed)

	_continue_button.pressed.connect(_on_save_pressed)
	_exit_button.pressed.connect(_on_exit_pressed)


func _on_visibility_changed():
	if visible and INPUTSTATE.get_is_input_via_joypad():
		_continue_button.grab_focus.call_deferred()


func _on_save_pressed() -> void:
	continued.emit()


func _on_exit_pressed() -> void:
	exited.emit()
