extends PanelContainer


@onready var _main_menu_screen: MainPauseMenuScreen = %MainPauseMenuScreen


func _ready():
	hide()

	_main_menu_screen.continued.connect(_on_main_continued)
	_main_menu_screen.exited.connect(_on_main_exited)


func _unhandled_input(_event) -> void:
	if Input.is_action_just_pressed('pause_menu'):
		if visible:
			get_tree().paused = false
			hide()
		else:
			get_tree().paused = true
			_main_menu_screen.show()
			show()


func _on_main_continued() -> void:
	get_tree().paused = false
	hide()


func _on_main_exited() -> void:
	get_tree().paused = false
	SCENE.goto_scene(ID.SCENE_TYPES.MENU)
