class_name State extends Node


# INTERNAL STATE DATA:

var rng := RandomNumberGenerator.new()
var _logger: Logger = LOGGING.get_logger('STATE')


func reset() -> void:
	rng.randomize()


# STATE DATA GETTERS:



# SIGNALS AND HANDLERS:



# HELPER METHODS:

# vararg is not supported in GDscript (yet) :/

func _log_signal_0(__signal_name: String) -> void:
	_logger.info('got signal %s'%[
		__signal_name
	])

func _log_signal_1(__arg1, __signal_name: String) -> void:
	_logger.info('got signal %s with argument %s'%[
		__signal_name, __arg1
	])

func _log_signal_2(__arg1, __arg2, __signal_name: String) -> void:
	_logger.info('got signal %s with arguments %s , %s'%[
		__signal_name, __arg1, __arg2
	])

func _log_signal_3(__arg1, __arg2, __arg3, __signal_name: String) -> void:
	_logger.info('got signal %s with arguments %s , %s , %s'%[
		__signal_name, __arg1, __arg2, __arg3
	])

func _log_signal_4(__arg1, __arg2, __arg3, __arg4, __signal_name: String) -> void:
	_logger.info('got signal %s with arguments %s , %s , %s , %s'%[
		__signal_name, __arg1, __arg2, __arg3, __arg4
	])

func _log_signal_5(__arg1, __arg2, __arg3, __arg4, __arg5, __signal_name: String) -> void:
	_logger.info('got signal %s with arguments %s , %s , %s , %s , %s'%[
		__signal_name, __arg1, __arg2, __arg3, __arg4, __arg5
	])


func _ready():	
	# Automatically connect _on_ handler methods to signals:
	for method_dict in get_method_list():
		var method_name = method_dict['name']
		if not method_name.begins_with('_on_'):
			continue

		var signal_name = method_name.substr(4) # len('_on_') = 4
		if !has_signal(signal_name):
			_logger.error('No matching signal defined for handler method %s! You should add a %s signal.'%[
				method_name, signal_name
			])
			continue

		connect(signal_name, Callable(self, method_name))

	for signal_dict in get_signal_list():
		match len(signal_dict.args):
			0:
				connect(signal_dict['name'], Callable(self, '_log_signal_0').bind(signal_dict['name']))
			1:
				connect(signal_dict['name'], Callable(self, '_log_signal_1').bind(signal_dict['name']))
			2:
				connect(signal_dict['name'], Callable(self, '_log_signal_2').bind(signal_dict['name']))
			3:
				connect(signal_dict['name'], Callable(self, '_log_signal_3').bind(signal_dict['name']))
			4:
				connect(signal_dict['name'], Callable(self, '_log_signal_4').bind(signal_dict['name']))
			5:
				connect(signal_dict['name'], Callable(self, '_log_signal_5').bind(signal_dict['name']))
			_:
				_logger.warning('Too many args for signal %s, please add an appropriate _log_signal_%d func.'%[
					signal_dict['name'], len(signal_dict.args)
				])
				connect(signal_dict['name'], Callable(self, '_log_signal_0').bind(signal_dict['name']))
 
