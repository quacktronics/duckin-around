extends Node


"""
We use this instead of SceneTree.change_scene_to_file, because that method would also replace our singletons.
"""


signal removed_old_scene(old_scene_id: ID.SCENE_TYPES, new_scene_id: ID.SCENE_TYPES)
signal switched_scene(old_scene_id: ID.SCENE_TYPES, new_scene_id: ID.SCENE_TYPES)


var scene_paths = {
	ID.SCENE_TYPES._TEST_MINIGAME: 'res://scenes/_test_minigame/test_minigame.tscn',
	ID.SCENE_TYPES.MENU: 'res://scenes/menu/menu.tscn',
	ID.SCENE_TYPES.SETTING: 'res://components/settings/settings.tscn',
	ID.SCENE_TYPES.STORY_WALKING: 'res://scenes/story/walking.tscn'
}

var _logger: Logger = LOGGING.get_logger('SCENE')

var _current_scene_id = null
var _current_scene = null


func _ready():
	var root = get_tree().get_root()
	_current_scene = root.get_child(root.get_child_count() - 1)


func goto_scene(__scene_id: ID.SCENE_TYPES):
	# Deleting the current scene at this point is
	# a bad idea, because it may still be executing code.
	# So we have to give it some time.
	call_deferred("__deferred_goto_scene", __scene_id)


func __deferred_goto_scene(__scene_id: ID.SCENE_TYPES):
	var old_scene_id = _current_scene_id if _current_scene_id else -1

	_current_scene.free()

	removed_old_scene.emit(old_scene_id, __scene_id)

	var new_scene: PackedScene = ResourceLoader.load(scene_paths[__scene_id])
	_current_scene_id = __scene_id
	_current_scene = new_scene.instantiate()

	get_tree().get_root().add_child(_current_scene)
	get_tree().set_current_scene(_current_scene)

	_logger.info('switched to scene #%s loaded from %s'%[ __scene_id, scene_paths[__scene_id] ])

	switched_scene.emit(old_scene_id, __scene_id)
