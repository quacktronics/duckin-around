extends Node


const BASE_PATH = 'res://assets/data'


# loosely based on Dance Dance Revolution / In the groove
# see https://www.reddit.com/r/DanceDanceRevolution/comments/4ay7kh/comment/d14jr00
# in milliseconds
var score_windows = [
    40,
    95,
    140,
]

var score_utterances = [
    [ 'Perfect!',   'Awesome!',     'Quacktastic!',     'Beak-taking!',     'Incredible!'   ],
    [ 'Good',       'Great',        'Nice',             'Sweet',            'Cool'          ],
    [ 'Okay...',    'Decent...',    'Not bad...',       'Alright...',       'Fine...'       ]
]

var missed_utterance = 'Miss!'

var too_early_utterance = 'Too early'
var hit_too_early_utterance = 'A bit too early'

var too_late_utterance = 'Too late'
var hit_too_late_utterance = 'A bit too late'

var score_values = [
    100,
    50,
    30
]

var rating_windows = [
    90,
    75,
    60,
    45,
    30,
    0
]

var rating_grades = [
    'QQ',
    'Q',
    'A',
    'B',
    'C',
    'F'
]

var rating_ducklings = [
    2,
    2,
    1,
    1,
    1,
    0
]
