extends Node


signal input_changed_to_joypad()
signal input_changed_to_keyboard()


var _was_last_input_via_joypad = false
var _logger = LOGGING.get_logger('INPUTSTATE')


func _ready():
    for joypad_id in Input.get_connected_joypads():
        var joypad_name = Input.get_joy_name(joypad_id)
        _logger.info('found device: "%s" id=%d guid=%s'%[
            joypad_name, joypad_id, Input.get_joy_guid(joypad_id)
        ])
        if joypad_name.contains('XInput'):
            # Godot also recognizes e.g. my SpaceNavigator mouse as a joypad
            # todo: check if there is a better way for this, this only supports Xbox Controllers
            _was_last_input_via_joypad = true
            break

    _logger.info('initial state: input via joypad = %s'%_was_last_input_via_joypad)


func _input(__event: InputEvent):
    var is_input_via_joypad = (
        __event is InputEventJoypadButton
        or __event is InputEventJoypadMotion
    )

    if not _was_last_input_via_joypad and is_input_via_joypad:
        input_changed_to_joypad.emit()
    elif _was_last_input_via_joypad and not is_input_via_joypad:
        input_changed_to_keyboard.emit()
    else:
        return

    _was_last_input_via_joypad = is_input_via_joypad
    _logger.info('changed state: input via joypad = %s'%is_input_via_joypad)


func get_is_input_via_joypad() -> bool:
    return _was_last_input_via_joypad
